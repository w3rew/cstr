/*!
 \file cstr.h In this file cstr functions are declared
 */
#ifndef CSTR_H
#define CSTR_H
#include <stdio.h>
#include <stdlib.h>
int my_puts(const char *str);
const char *strchr(const char *str, int chr);
size_t strlen(const char *str);
char *strcpy(char *destination, const char *source);
char *strncpy(char *destination, const char *source, size_t num);
char *strcat(char *destination, const char *source);
char *strncat(char *destination, const char *source, size_t num);
char *strdup(const char *str1);
char *my_fgets(char *str, int num, FILE *stream);
ssize_t my_getline(char **lineptr, size_t *n, FILE *stream);
#endif
