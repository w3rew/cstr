/*!
 \file cstr.c i this file c string functions are implemented
 */
#include <stdlib.h>
#include <stdio.h>
#include "cstr.h"
#include <assert.h>
/*!
 * \brief Takes C string and prints it to stdout
 * \param[in] str pointer to the C string
 * \return EOF if an error happened, 0 otherwise
 */
int my_puts(const char *str) {
    for (;*str != '\0'; ++str) {
        int written = putc(*str, stdout);
        if (written < 0) return EOF;
    }
    return (putc('\n', stdout) >= 0 ? 0 : EOF);
}
/*!
 * \brief Locates a character in the C string
 * \param[in] str a C string to find character in
 * \param[in] chr a character to find
 * \return a pointer to located character. If there is no such character NULL pointer is returned.
 *
 * Note that you can locate '\0' symbol as well.
 */
const char  *strchr(const char *str, int chr) {
    for (; *str != '\0'; ++str) if (*str == chr) return str;

    if (chr == '\0') return str;

    return NULL;
}
/*!
 * \brief Gets the length of the C string
 * \param[in] str a C string
 * \return length of the string
 * \warning '\0' is not considered a part of the string
 */
size_t strlen(const char *str){
    return (size_t) (strchr(str, '\0') - str);
}
/*!
 * \brief Copies one string into another
 * \param[in] dest the destination string
 * \param[in] src the source string
 * \return dest pointer
 *
 * Copies src C string into dest. The start of src is placed at the dest pointer. 
 * '\0' is also copied. Dest contents are erased and '\0' is ignored, so dest doesn't have to be a C string.
 * \warning dest viewed as array should be sized at least strlen(src) + 1 because of terminating null.
 */
char *strcpy(char *dest, const char *src) {
    char *dest_iterator = dest;
    for (;*src != '\0'; ++src) {
        *dest_iterator = *src;
        ++dest_iterator;
    }
    *dest_iterator = '\0';
    return dest;
}

/*!
 * \brief Copies one string into another, but not more than num symbols
 * \param[in] dest the destination string
 * \param[in] src the source string
 * \param[in] num number of characters to copy
 * \return dest pointer
 *
 * Copies src C string in dest until num symbol is copied or src ends.  If src ends before num symbols are copied,
 * the rest is filled with zeroes.  Note that '\0' is 
 * considered a symbol, so in order for dest to be a C string num should be at least strlen(src) + 1.
 *
 * \warning dest might not be a C string because '\0' is not implicitly added to the end of dest if strlen(src) + 1 > num
 * \warning dest and src shall not overlap.
 */
char *strncpy(char *dest, const char *src, size_t num) {
    bool flag = false;
    for (size_t i = 0; i < num; ++i) {
        if (!flag) dest[i] = src[i];
        else dest[i] = 0;

        if (src[i] == '\0') flag = true;
    }
    return dest;
}


/*!
 * \brief concatenates two strings
 * \param[in] dest the first string
 * \param[in] src the second string
 * \return dest pointer
 *
 * Appends src to dest. The first symbol of src overwrites the terminating '\0' of dest, and the '\0' is added to the end of dest.
 *
 * \warning dest and src shall not overlap
 */
char *strcat(char *dest, const char *src) {
    char *dest_end = &dest[strchr(dest, '\0') - dest];
    strcpy(dest_end, src);
    return dest;
}
/*!
 * \brief appends not more than num symbols from one string to another
 * \param[in] dest the first string
 * \param[in] src the second string
 * \param[in] num number of symbols to append
 * \return dest pointer
 *
 * Appends num symbols of  src to dest and adds '\0' to the end.
 * The first symbol of src overwrites the terminating '\0' of dest, and the '\0' is added to the end of dest.
 * If strlen(src) + 1 < num, src is fully appended to dest and the rest of the space is left untouched.
 *
 * \warning dest and src shall not overlap
 */
char *strncat(char *dest, const char *src, size_t num) {
    char *dest_end = &dest[strchr(dest, '\0') - dest];
    size_t i = 0;
    for (; i < num; ++i) {
        if (src[i] == '\0') break;
        dest_end[i] = src[i];
    }
    dest_end[i] = '\0';
    return dest;
}
    
/*!
 * \brief duplicates a string
 *param[in] str1 a string to duplicate
 \return a pointer to the duplicate of str1. If an error occurs, returns NULL pointer.

 \warning returned string should be set free() after usage as it lies in dynamic memory.
 \warning dest and src shall not overlap
 */
char *strdup(const char *str1) {
    char *str2 = (char*) malloc(strlen(str1) + 1);
    if (str2 == NULL) return NULL;

    strcpy(str2, str1);
    return str2;
}
/*!
  \brief gets not more than num - 1 characters from the stream and stores them in the string
  \param[in] str a string where extracted characters will be stored
  \param[in] num maximum number of characters to extract
  \param[in] stream a stream out of which characters will be acquired
  \return NULL pointer if file error occurred or EOF occurred before reading the first character. \
  If there were no errors or an EOF error after reading at least one character, str pointer is returned.
 
  Reads stream until either:
  - num - 1 characters is read
  - '\n' character is read
  - EOF occurs
  - an error happens

  '\n' is considered a valid character and added to the string. After writing to str, terminating '\0' is placed.
 */
char *my_fgets(char *str, int num, FILE *stream) {
    int i = 0;
    for (; i < num - 1; ++i) {
        int read_value = getc(stream);
        if (read_value == EOF) {
            if (i == 0) return NULL;
            else {
                if (ferror(stream)) return NULL;
                else return str;
            }
        } else {
            str[i] = (char) read_value;
            if (read_value == '\n') break;
        }

    }
    str[i + 1] = '\0';
    return str;
}
/*!
  \brief reads one line from the stream, adjusting the target string if needed
  \param[in] lineptr a pointer to the target string. It should point at a pointer which can be an argument of free()
  \param[in] n a pointer to the size of the string. Either *lineptr should be NULL, either *n should be non-zero.
  \param[in] stream input stream
  \return -1 if an error has occurred or if we approached EOF before reading any character. Number of read characters otherwise.

  Reads symbols from stream until approaches EOF or '\n' and writes them to lineptr. If lineptr can't contain the data,
  reallocates it properly. *lineptr is updated then. Adds '\0' to the end. 
*/
ssize_t my_getline(char **lineptr, size_t *n, FILE *stream) {
    assert(n != NULL);
    if (*lineptr == NULL ){
        if (*n > 0) *lineptr = (char*) malloc(*n); 
        else  {
            *lineptr = (char*) malloc(1);
            *n = 1;
        }
    }
    assert(*n > 0);
    ssize_t written = 0;
    while (true) {
        int read_character = getc(stream);
        if (read_character < 0) {  //if EOF is encountered, we should return -1 if we haven't read anything
            if (written == 0) return -1;
            break;
        }
        if ((size_t)(written) + 1uL >= *n ) { //Reallocate enough memory to store written characters + '\0'
            char *reallocated_ptr = (char*) realloc(*lineptr, 2* (*n));
            if (reallocated_ptr == NULL) return -1;
            *lineptr = reallocated_ptr;
            (*n) *= 2;
        }
        (*lineptr)[written] = (char)read_character;
        ++written;
        if (read_character == '\n') break;
    }
    (*lineptr)[written] = '\0';
    return written;
}

