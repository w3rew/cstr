/*!
 \file tests.h in this file some testing facilities are provided. This file should be included after correct defines in test*.c
 */
/*!
  In this function we test strchr, strlen, strcpy, strncpy, strcat, strncat, strdup by performing simple operations
  on strings and observing the output
 */
int make_tests(){
    char str[] = "My test string";
    printf("%s\n", str);
    printf("The position of character 'e' is %ld\n", _my_strchr(str, 'e') - str);
    printf("The position of character 'w' is NULL: %s\n", (_my_strchr(str, 'w') == NULL) ? "true" : "false");
    printf("The length of the string is %ld\n", (long int) _my_strlen(str));
    char copy_of_str[40];
    _my_strcpy(copy_of_str, str);
    printf("This is the copy of str:\n%s\nand its length: %ld\n", copy_of_str, (long int) _my_strlen(copy_of_str));
    _my_strncpy(copy_of_str, "Test", 5);
    printf("strncpy works correct: %s\n", (copy_of_str[0] == 'T' && copy_of_str[1] == 'e' && copy_of_str[2] == 's' && 
                copy_of_str[3] == 't' && copy_of_str[4] =='e') ? "true" : "false");
    char another_str[40];
    _my_strcpy(another_str, str);
    _my_strcat(another_str, str);
    printf("This is another string:\n%s\n", another_str);
    _my_strncat(another_str, str, 4ul);
    printf("This is another string:\n%s\n", another_str);
    char *one_more_string = _my_strdup(another_str);
    printf("This is duplicated string:\n%s\n", one_more_string);
    free(one_more_string);
    _my_puts("This line has been printed by puts\n");
    size_t line_size = 40;
    char *line_from_getline = NULL;
    printf("Read %ld characters\n", _my_getline(&line_from_getline, &line_size, stdin));
    printf("%s", line_from_getline);

    return 0;
}

