#!/usr/bin/bash

./test.o < tests_input > test.txt
./reference.o < tests_input > reference.txt
diff -s test.txt reference.txt > tests.log
rm test.txt reference.txt
