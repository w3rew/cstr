var searchData=
[
  ['main_21',['main',['../tests__cstr_8c.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'tests_cstr.c']]],
  ['make_5ftests_22',['make_tests',['../tests_8h.html#a5722d85e5127e70608c9c7833f122db9',1,'tests.h']]],
  ['my_5ffgets_23',['my_fgets',['../cstr_8c.html#a86ba38e780bbb34040c7d731c44a2396',1,'my_fgets(char *str, int num, FILE *stream):&#160;cstr.c'],['../cstr_8h.html#a86ba38e780bbb34040c7d731c44a2396',1,'my_fgets(char *str, int num, FILE *stream):&#160;cstr.c']]],
  ['my_5fgetline_24',['my_getline',['../cstr_8c.html#a0fd781fafc60ee3a86d011c780c6ac02',1,'my_getline(char **lineptr, size_t *n, FILE *stream):&#160;cstr.c'],['../cstr_8h.html#a0fd781fafc60ee3a86d011c780c6ac02',1,'my_getline(char **lineptr, size_t *n, FILE *stream):&#160;cstr.c']]],
  ['my_5fputs_25',['my_puts',['../cstr_8c.html#a613f18cc4b2e7c9aa4464df1431e8f66',1,'my_puts(const char *str):&#160;cstr.c'],['../cstr_8h.html#a613f18cc4b2e7c9aa4464df1431e8f66',1,'my_puts(const char *str):&#160;cstr.c']]]
];
