# C string library
This library includes standart C string functions as described in C specifications. Implemented functions are:

- puts
- strchr
- strlen
- strcpy
- strncpy
- strcat
- strncat
- strdup
- fgets
- getline

There are two files for testing purposes, tests_cstr.c and tests_reference.c. The first one produces output using our library.
The second one uses standard string.h. Then their outputs are compared using diff.

