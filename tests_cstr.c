/*!
 \file tests_cstr.c in this file we implement testing for cstr.h using tests from tests.h
 */
#include <stdlib.h>
#include "cstr.h"
#include <stdio.h>

#define _my_puts my_puts
#define _my_strchr strchr
#define _my_strlen strlen
#define _my_strcpy strcpy
#define _my_strncpy strncpy
#define _my_strcat strcat
#define _my_strncat strncat
#define _my_strdup strdup
#define _my_fgets my_fgets
#define _my_getline my_getline
#include "tests.inc"
/*!
  In main function we use tests from tests.h
 */

int main() {
    return make_tests();
}

